package DAO;


import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import Metiers.Ceinture;

public class CeintureDAO extends DAO<Ceinture> {

	
	private static final String TABLE_CEINTURE = "ceinture";
	private static final String COL_ID = "idCeinture";
	private static final String COL_DATE_ENTREE = "dateEntree";
	private static final String COL_DATE_SORTIE = "dateSortie";
	private static final String COL_ID_COULEUR = "idCouleur";
	

	public void open (SQLiteBaseJudoStock gn){
		this.bdd = gn.getWritableDatabase();
	}
	
	public void close(){
		this.bdd.close();
	}
	
	
	
	@Override
	public Ceinture create(Ceinture uneCeinture) {
		//Cr�ation d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String entree = formatter.format(uneCeinture.getDateEntree());
		String sortie = formatter.format(uneCeinture.getDateSortie());
		
		//on lui ajoute une valeur associ�e � une cl� (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
		values.put(COL_DATE_ENTREE, entree);
		values.put(COL_DATE_SORTIE, sortie);
		values.put(COL_ID_COULEUR, uneCeinture.getIdCouleur());
						
		//on ins�re l'objet dans la BDD via le ContentValues
		long id = this.bdd.insert(TABLE_CEINTURE, null, values);
		uneCeinture.setIdCeinture(id);
						
		return uneCeinture;
	}
	

	@Override
	public Ceinture update(Ceinture uneCeinture) {
		ContentValues values = new ContentValues();
		
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String entree = formatter.format(uneCeinture.getDateEntree());
		String sortie = formatter.format(uneCeinture.getDateSortie());
		
		
		values.put(COL_DATE_ENTREE, entree);
		values.put(COL_DATE_SORTIE, sortie);
		values.put(COL_ID_COULEUR, uneCeinture.getIdCouleur());
		
		bdd.update(TABLE_CEINTURE, values, COL_ID + " = " + uneCeinture.getIdCeinture(), null);
		
		return uneCeinture;
	}

	@Override
	public void delete(Ceinture uneCeinture) {
		bdd.delete(TABLE_CEINTURE, COL_ID + " = " + uneCeinture.getIdCeinture(), null);
		
	}
	
	
	@Override
	public Ceinture get(long id) {
		Cursor c = bdd.query(TABLE_CEINTURE, new String[] {COL_ID,  COL_DATE_ENTREE, COL_DATE_SORTIE, COL_ID_COULEUR}, COL_ID + "=" + id, null, null, null, null);
		if (c.getCount() == 0) return null;
		c.moveToFirst();
		Ceinture uneCeinture = new Ceinture(c.getInt(0), c.getDate(1), c.getDate(2), c.getInt(3));
		c.close();
		return uneCeinture;
	}
	
	
	public ArrayList <Ceinture> getAll() {
		Cursor c = bdd.query(TABLE_CEINTURE, new String[] {COL_ID, COL_DATE_ENTREE, COL_DATE_SORTIE, COL_ID_COULEUR}, null , null, null, null, null);
		if (c.getCount() == 0) return null;
		ArrayList <Ceinture> lesCeintures = new ArrayList <Ceinture>();
		 
		while(c.moveToNext())
        {
			Ceinture uneCeinture = new Ceinture(c.getInt(0), c.getDate(1), c.getDate(2), c.getInt(3));
			lesCeintures.add(uneCeinture);
        }
		c.close();
		return lesCeintures;
	}
}
