package DAO;

import android.content.ContentValues;
import Metiers.Couleur;

public class CouleurDAO extends DAO<Couleur> {

	private static final String TABLE_COULEUR = "couleur";
	private static final String COL_ID = "idCouleur";
	private static final String COL_LIBELLE = "libelle";
	private static final String COL_NBSTOCK = "nbstock";
	
	
	public void open (SQLiteBaseJudoStock gn){
		this.bdd = gn.getWritableDatabase();
	}
	
	public void close(){
		this.bdd.close();
	}
	
	@Override
	public Couleur get(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Couleur create(Couleur uneCouleur) {
		//Cr�ation d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();

		//on lui ajoute une valeur associ�e � une cl� (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
		values.put(COL_LIBELLE, uneCouleur.getLibelle());
		values.put(COL_NBSTOCK, uneCouleur.getNbStock());
						
		//on ins�re l'objet dans la BDD via le ContentValues
		long id = this.bdd.insert(TABLE_COULEUR, null, values);
		uneCouleur.setIdCouleur(id);
						
		return uneCouleur;
	}

	@Override
	public Couleur update(Couleur uneCouleur) {
		ContentValues values = new ContentValues();
		
		values.put(COL_LIBELLE, uneCouleur.getLibelle());
		values.put(COL_NBSTOCK, uneCouleur.getNbStock());
		
		bdd.update(TABLE_CLASSE, values, COL_ID + " = " + uneClasse.getIdClasse(), null);
		
		return uneClasse;
	}

	@Override
	public void delete(Couleur obj) {
		// TODO Auto-generated method stub
		
	}

}
