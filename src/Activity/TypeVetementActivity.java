package Activity;

import com.NCorp.gestiondesnotes.IHM.ClasseAdapter;
import com.NCorp.gestiondesnotes.Metier.Classe;

import DAO.TypeVetementDAO;
import Metiers.TypeVetement;
import android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;


public class TypeVetementActivity extends Activity{
	
	private Context Context = this;
	private TypeVetementDAO DAOTV;
	
	public boolean onChildClick(ExpandableListView parent, View v,
            int groupPosition, int childPosition, long id){
        
        return false;
    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_typeVetement);
				
		setTitle("Gestion des Stocks : Gestion des Types de Vetements");
		
		android.app.ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2A4FBF")));
        
        DAOTV = new TypeVetementDAO(this);
        DAOTV.open();
        refresh_list();        
        
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_action_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

	public void AlertAjout(){
		LayoutInflater inflater = getLayoutInflater();
		View promptsView = inflater.inflate(R.layout.dialog_ajout_typeVetement, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Context);
			// set title
			alertDialogBuilder.setTitle("Ajouter un Type de Vetement");
 
			// set dialog message
			alertDialogBuilder
				.setView(promptsView);
			final EditText eTNomClasse = (EditText) promptsView
					.findViewById(R.id.eT_classe_nom);
			final EditText eTNbEleve = (EditText) promptsView
					.findViewById(R.id.eT_classes_nb);
			
			
			alertDialogBuilder
				.setPositiveButton("Ajouter",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						Context context = getApplicationContext();
						int duration = Toast.LENGTH_LONG;

						
						if(!(eTNomClasse.getText().length() == 0) && !(eTNbEleve.getText().length() == 0)){
							TypeVetement tv= new TypeVetement( DAOTV.getLastIdTypeVetement(),eTNomTypeVetement.getText().toString() , Integer.parseInt(eTNbVetement.getText().toString()));
							DAOTV.create(tv);
							refresh_list();

							CharSequence text = "Type de Vetement Ajout�!";
							Toast toast = Toast.makeText(context, text, duration);
							toast.show();
						}else{
							CharSequence text = "Champs vide!";
							Toast toast = Toast.makeText(context, text, duration);
							toast.show();
						}
						
					}

				  })
				.setNegativeButton("Annuler",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
				
		}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	public void refresh_list(){

        ListView listContent = (ListView)findViewById(R.id.eLVClasse);
		ClasseAdapter adapter = new ClasseAdapter(this, 
	                R.layout.list_classes, DAOC.read());
	    listContent.setAdapter(adapter);
	    listContent.setOnItemClickListener(OnClickItem);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////
public OnItemClickListener OnClickItem = new OnItemClickListener(){
		
		@Override
        public void onItemClick(AdapterView<?> parent, View view,
           int position, long id) {

			final TypeVetement tv = DAOTV.readAtCursor(position);

			LayoutInflater inflater = getLayoutInflater();
			View promptsView = inflater.inflate(R.layout.dialog_ajout_typeVetement, null);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					Context);
				// set title
				alertDialogBuilder.setTitle("Modifier/Supprimer une classe");
	 
				// set dialog message
				alertDialogBuilder
					.setView(promptsView);
				final EditText eTNomClasse = (EditText) promptsView
						.findViewById(R.id.eT_classe_nom);
				final EditText eTNbEleve = (EditText) promptsView
						.findViewById(R.id.eT_classes_nb);
				
				eTNomClasse.setText(cl.getLibClasse());
				eTNbEleve.setText(Integer.toString(tv.getNbClasse()));
				
				alertDialogBuilder
					.setPositiveButton("Modifier",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Context context = getApplicationContext();
							int duration = Toast.LENGTH_LONG;
							if(!(eTNomTypeVetement.getText().length() == 0) && !(eTNbVetement.getText().length() == 0)){
								TypeVetement ma= new TypeVetement( tv.getIdTypeVetement(),eTNomTypeVetement.getText().toString() , Integer.parseInt(eTNbEleve.getText().toString()));
								DAOTV.update(ma);
								refresh_list();

								CharSequence text = "Type de vetement modifi�e!";
								Toast toast = Toast.makeText(context, text, duration);
								toast.show();
							}else{
								CharSequence text = "Champs vide!";
								Toast toast = Toast.makeText(context, text, duration);
								toast.show();
							}
						}

					  })
					.setNegativeButton("Annuler",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					})
					.setNeutralButton("Supprimer",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									Context);
								// set title
								alertDialogBuilder.setTitle("Voulez-vous vraiment supprimer ce type de vetement?");
					 
								// set dialog message
								alertDialogBuilder
									.setCancelable(false)
									.setPositiveButton("Oui",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id) {
											Context context = getApplicationContext();
											int duration = Toast.LENGTH_LONG;
											TypeVetement ma= new TypeVetement( tv.getIdTypeVetement(),tv.getLibTypeVetement() , tv.getNbTypeVetement());
											DAOTV.delete(ma);
											refresh_list();
											
											CharSequence text = "Type supprim�!";
											Toast toast = Toast.makeText(context, text, duration);
											toast.show();
										}
									  })
									.setNegativeButton("Non",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id) {
											dialog.cancel();
										}
									});
					 
									// create alert dialog
									AlertDialog alertDialog = alertDialogBuilder.create();
					 
									// show it
									alertDialog.show();
						}
					});
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();
			
			
        }
		
	};
}
