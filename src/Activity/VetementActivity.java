package Activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;




import com.NCorp.gestiondesnotes.DAO.EleveDAO;

import DAO.TypeVetementDAO;
import DAO.VetementDAO;
import Metiers.TypeVetement;
import Metiers.Vetement;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class VetementActivity extends Activity {
	
	private Context Context = this;
	private VetementDAO DAOV;
	private int year;
	private int month;
	private int day;
	protected Date date;
	
	public boolean onChildClick(ExpandableListView parent, View v,
            int groupPosition, int childPosition, long id){
        
        return false;
    }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eleve);
			
		setTitle("Gestion des Notes : Gestion des Eleves");
		
		android.app.ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#21A635")));
        
        DAOV = new VetementDAO(this);
        DAOV.open();
        refresh_list();           
	}
	 @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.main_action_action, menu);
	        return super.onCreateOptionsMenu(menu);
	    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle presses on the action bar items
	        switch (item.getItemId()) {
	            case R.id.action_ajouter:
	                AlertAjout();
	                return true;
	            default:
	                return super.onOptionsItemSelected(item);
	        }
	    }
		
}
