package Metiers;

import java.util.Date;

public class Consommable {

	private long idConsommable;
	private Date DLC; 
	private TypeConsommable unType;
	private long idType;
	
	
	public Consommable(long idConsommable, Date dLC, TypeConsommable unType,
			long idType) {
		super();
		this.idConsommable = idConsommable;
		DLC = dLC;
		this.unType = unType;
		this.idType = idType;
	}


	public long getIdConsommable() {
		return idConsommable;
	}


	public void setIdConsommable(long idConsommable) {
		this.idConsommable = idConsommable;
	}


	public Date getDLC() {
		return DLC;
	}


	public void setDLC(Date dLC) {
		DLC = dLC;
	}


	public TypeConsommable getUnType() {
		return unType;
	}


	public void setUnType(TypeConsommable unType) {
		this.unType = unType;
	}


	public long getIdType() {
		return idType;
	}


	public void setIdType(long idType) {
		this.idType = idType;
	}
	
	
}
