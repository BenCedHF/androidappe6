package Metiers;

public class Couleur {

	private long idCouleur;
	private String libelle;
	private int nbstock;
	
	
	public Couleur(long idCouleur, String libelle, int nbstock)
	{
		this.idCouleur = idCouleur;
		this.libelle = libelle;
		this.nbstock = nbstock;
	}
	
	public Couleur(String libelle, int nbstock)
	{
		this.libelle = libelle;
		this.nbstock = nbstock;
	}
	
	
	public long getIdCouleur() {
		return idCouleur;
	}
	
	
	public void setIdCouleur(long idCouleur) {
		this.idCouleur = idCouleur;
	}
	
	public String getLibelle() {
		return libelle;
	}
	
	
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
	public int getNbStock() {
		return nbstock;
	}
	
	
	public void setNbSock(int nbstock) {
		this.nbstock = nbstock;
	}
		
	
}
