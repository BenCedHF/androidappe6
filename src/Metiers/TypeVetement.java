package Metiers;

public class TypeVetement {
	
	private long idTypeVetement;
	private String libelle;
	private int seuil;
	private int nbstock;
	
	
	public TypeVetement(long idTypeVetement, String libelle, int seuil,
			int nbstock) {
		super();
		this.idTypeVetement = idTypeVetement;
		this.libelle = libelle;
		this.seuil = seuil;
		this.nbstock = nbstock;
	}


	public TypeVetement(String libelle, int seuil, int nbstock) {
		super();
		this.libelle = libelle;
		this.seuil = seuil;
		this.nbstock = nbstock;
	}


	public long getIdTypeVetement() {
		return idTypeVetement;
	}


	public void setIdTypeVetement(long idTypeVetement) {
		this.idTypeVetement = idTypeVetement;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public int getSeuil() {
		return seuil;
	}


	public void setSeuil(int seuil) {
		this.seuil = seuil;
	}


	public int getNbstock() {
		return nbstock;
	}


	public void setNbstock(int nbstock) {
		this.nbstock = nbstock;
	}
		

}
