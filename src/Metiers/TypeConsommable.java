package Metiers;

public class TypeConsommable {

	private long idConsommable;
	private String libelle;
	private String nbStock;
	
	
	public TypeConsommable(long idConsommable, String libelle, String nbStock) {
		super();
		this.idConsommable = idConsommable;
		this.libelle = libelle;
		this.nbStock = nbStock;
	}


	public long getIdConsommable() {
		return idConsommable;
	}


	public void setIdConsommable(long idConsommable) {
		this.idConsommable = idConsommable;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public String getNbStock() {
		return nbStock;
	}


	public void setNbStock(String nbStock) {
		this.nbStock = nbStock;
	}
	
	
}
