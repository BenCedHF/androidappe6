package Metiers;

import java.util.Date;

public class Ceinture {

	private long idCeinture;
	private Date dateEntree;
	private Date dateSortie;
	private Couleur uneCouleur;
	private long idCouleur;
	
	
	public Ceinture(long idCeinture, Date dateEntree, Date dateSortie,
		   Couleur uneCouleur, long idCouleur) {
		
		this.idCeinture = idCeinture;
		this.dateEntree = dateEntree;
		this.dateSortie = dateSortie;
		this.uneCouleur = uneCouleur;
		this.idCouleur = idCouleur;
	}


	public long getIdCeinture() {
		return idCeinture;
	}


	public void setIdCeinture(long idCeinture) {
		this.idCeinture = idCeinture;
	}


	public Date getDateEntree() {
		return dateEntree;
	}


	public void setDateEntree(Date dateEntree) {
		this.dateEntree = dateEntree;
	}


	public Date getDateSortie() {
		return dateSortie;
	}


	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}


	public Couleur getUneCouleur() {
		return uneCouleur;
	}


	public void setUneCouleur(Couleur uneCouleur) {
		this.uneCouleur = uneCouleur;
	}


	public long getIdCouleur() {
		return idCouleur;
	}


	public void setIdCouleur(long idCouleur) {
		this.idCouleur = idCouleur;
	}
	
		
}
