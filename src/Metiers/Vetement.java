package Metiers;

import java.util.Date;

public class Vetement {

	private long idVetement;
	private Date dateEntree;
	private Date dateSortie;
	private TypeVetement unType;
	private long idType;
	
	
	public Vetement(long idVetement, Date dateEntree, Date dateSortie,
			TypeVetement unType, long idType) {
		
		this.idVetement = idVetement;
		this.dateEntree = dateEntree;
		this.dateSortie = dateSortie;
		this.unType = unType;
		this.idType = idType;
	}


	public long getIdVetement() {
		return idVetement;
	}


	public void setIdVetement(long idVetement) {
		this.idVetement = idVetement;
	}


	public Date getDateEntree() {
		return dateEntree;
	}


	public void setDateEntree(Date dateEntree) {
		this.dateEntree = dateEntree;
	}


	public Date getDateSortie() {
		return dateSortie;
	}


	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}


	public TypeVetement getUnType() {
		return unType;
	}


	public void setUnType(TypeVetement unType) {
		this.unType = unType;
	}


	public long getIdType() {
		return idType;
	}


	public void setIdType(long idType) {
		this.idType = idType;
	}
	
	
}
