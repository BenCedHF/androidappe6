
DROP TABLE IF EXISTS typevetement;
CREATE TABLE typevetement
(
	idTypeVetement INTEGER PRIMARY KEY AUTOINCREMENT,
	libelle CHAR(200) NOT NULL,
	seuil INTEGER NOT NULL,
	nbStock INTEGER NOT NULL
);


DROP TABLE IF EXISTS vetement;
CREATE TABLE vetement
(
	idVetement INTEGER PRIMARY KEY AUTOINCREMENT,
	taille VARCHAR(3),
	dateEntree DATE NOT NULL,
	dateSorie DATE NOT NULL,
	idType INTEGER NOT NULL,
	FOREIGN KEY (idType) REFERENCES typevetement(idTypeVetement)
);

